from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save

# https://stackoverflow.com/questions/44313667/django-using-instance-id-while-uploading-image
def get_image_path(instance, filename):
    return "profile/{id}/{file}".format(root=settings.MEDIA_URL, id=str(instance.user), file=filename)

# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    profile_image = models.ImageField(upload_to=get_image_path, blank=True, null=True)
    # profile_image = models.ImageField(upload_to='profile/', blank=True, null=True)

    # def save(self, *args, **kwargs):
    #     if self.user is None:
    #         saved_image = self.profile_image
    #         self.profile_image = None
    #         super(Profile, self).save(*args, **kwargs)
    #         self.profile_image = saved_image
    #         if 'force_insert' in kwargs:
    #             kwargs.pop('force_insert')

    #     super(Profile, self).save(*args, **kwargs)
    
    def __str__(self):
        return f"{self.user}"

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()
