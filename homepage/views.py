from django.shortcuts import render, redirect
from django.contrib import messages
from django.core import validators, serializers
from django.urls import reverse
from django.http import JsonResponse
from django.conf import settings

from .forms import SignUpForm
from .models import Profile, User

from django.contrib.auth.forms import AdminPasswordChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash, logout, login, authenticate
from django.contrib.auth.decorators import login_required

@login_required
def index(request):
    context = {
        'nav': [
            [reverse('homepage:index'), 'Home'],
            ['https://story5ppw-achmadafriza.herokuapp.com/', 'About Me'],
        ],
        'logged_in': True,
        'profile': Profile.objects.get(user=request.user).profile_image.url
    }

    print(request.user)
    print(Profile.objects.get(user=request.user).profile_image.url)
    print(settings.MEDIA_URL)
    print(settings.MEDIA_ROOT)

    return render(request, 'homepage.html', context)

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST, request.FILES)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.profile.profile_image = form.cleaned_data.get('profile_image')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('homepage:index')
    else:
        form = SignUpForm()
    
    context = {
        'nav': [
            [reverse('homepage:index'), 'Home'],
            ['https://story5ppw-achmadafriza.herokuapp.com/', 'About Me'],
        ],
        'logged_in': None,
        'form': form,
    }
    return render(request, 'signup.html', context)

# @login_required
# def settings(request):
#     pass