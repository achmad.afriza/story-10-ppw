from django import forms
from django.contrib.auth.models import User
from .models import Profile
from django.contrib.auth.forms import UserCreationForm

class SignUpForm(UserCreationForm):
    profile_image = forms.ImageField(allow_empty_file=True, required=False)

    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'username',
            'password1',
            'password2',
            'profile_image'
            )
        widgets = {
            'first_name': forms.TextInput(
                attrs={
                    'placeholder': 'First Name',
                    'class': 'statusform'
                }
            ),
            'last_name': forms.TextInput(
                attrs={
                    'placeholder': 'Last Name',
                    'class': 'statusform'
                }
            ),
            'email': forms.TextInput(
                attrs={
                    'placeholder': 'example@fuckyou.com',
                    'class': 'statusform'
                }
            ),
            'username': forms.TextInput(
                attrs={
                    'placeholder': 'Username',
                    'class': 'statusform'
                }
            ),
            'password1': forms.PasswordInput(
                attrs={
                    'placeholder': 'Password',
                    'class': 'statusform'
                }
            ),
            'password2': forms.PasswordInput(
                attrs={
                    'placeholder': 'Verify Password',
                    'class': 'statusform'
                }
            )
        }