# from django.http import HttpRequest
# from django.test import TestCase, LiveServerTestCase
# from django.urls import reverse, resolve
# from django.conf import settings
# from importlib import import_module
# from django.contrib import admin

# from .apps import HomepageConfig
# from .views import index
# # from .models import Book
# # from .forms import LikeBook

# from contextlib import contextmanager
# from selenium import webdriver
# from selenium.common.exceptions import WebDriverException
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait 
# from selenium.webdriver.support import expected_conditions
# import time
# from selenium.webdriver.support.expected_conditions import staleness_of

# # Create your tests here.
# class HomepageTests(TestCase):
#     def setUp(self):
#         # http://code.djangoproject.com/ticket/10899
#         settings.SESSION_ENGINE = 'django.contrib.sessions.backends.file'
#         engine = import_module(settings.SESSION_ENGINE)
#         store = engine.SessionStore()
#         store.save()
#         self.session = store
#         self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key
    
#     def test_apps(self):
#         self.assertEqual(HomepageConfig.name, 'homepage')
    
#     def test_homepage_status_code(self):
#         response = self.client.get('')
#         self.assertEquals(response.status_code, 200)

#     def test_view_url_by_name(self):
#         response = self.client.get(reverse('homepage:index'))
#         self.assertEquals(response.status_code, 200)

#     def test_view_uses_correct_template(self):
#         response = self.client.get(reverse('homepage:index'))
#         self.assertTemplateUsed(response, 'homepage.html')

#     def test_homepage_contains_correct_html(self):
#         response = self.client.get('')
#         self.assertContains(response, "<title>Afriza's StoryTen</title>")
    
#     def test_homepage_uses_correct_view(self):
#         func = resolve('/')
#         self.assertEqual(index, func.func)

# # Create your tests here.
# class Story10_FunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         super().setUp()
#         chrome_options = webdriver.ChromeOptions()
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('--disable-dev-shm-usage')
#         self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='./chromedriver')
    
#     def tearDown(self):
#         self.driver.quit()
#         super().tearDown()
    
#     def wait_for(self, fn):
#         MAX_WAIT = 10
#         start = time.time()
#         while True:
#             try:
#                 return fn()
#             except (AssertionError, WebDriverException) as e:
#                 if time.time() - start > MAX_WAIT:
#                     raise e
#                 time.sleep(0.5)
    
#     @contextmanager
#     def wait_for_page_load(self, timeout=30):
#         old_page = self.driver.find_element_by_tag_name('html')
#         yield
#         WebDriverWait(self.driver, timeout).until(
#             staleness_of(old_page)
#         )